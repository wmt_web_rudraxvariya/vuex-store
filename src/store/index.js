import { createStore } from "vuex";
import products from "./modules/products";
import randomUser from "./modules/randomuser";
import jokes from "./modules/jokes";
import profile from "./modules/profile";
export default createStore({
  state: {},
  mutations: {},
  actions: {},
  modules: {
    products,
    randomUser,
    jokes,
    profile,
  },
});
