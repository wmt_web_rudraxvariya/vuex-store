import axios from "axios";

const state = {
  Cjoke: "Try to edit and click submit",
  result: "Let's See",
  randomCars: [],
};
const getters = {
  getCurrentJoke: (state) => state,
};
const actions = {
  async getAllJoke({ commit }, jokeInp) {
    const res = await axios.get(
      "https://random-data-api.com/api/vehicle/random_vehicle"
    );
    commit("setCurrentState", jokeInp);
    commit("setApidata", res.data);
  },
};
const mutations = {
  setCurrentState(state, payload) {
    state.Cjoke = payload;
  },
  setApidata(state, payload) {
    state.randomCars = payload;
  },
};
export default {
  state,
  getters,
  actions,
  mutations,
};
