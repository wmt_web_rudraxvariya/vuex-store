import axios from "axios";
const state = {
  products: [],
};
const getters = { allProducts: (state) => state.products };
const actions = {
  async getProducts({ commit }) {
    console.log("api fetching..");
    const response = await axios.get("http://localhost:3000/products");
    commit("setProducts", response.data);
  },
  async addProducts({ commit }, product) {
    const response = await axios.post(
      "http://localhost:3000/products",
      product
    );
    commit("newProducts", response.data);
  },
  async deleteProduct({ commit }, productId) {
    await axios.delete(`http://localhost:3000/products/${productId}`);
    commit("removeProduct", productId);
  },
};
const mutations = {
  setProducts: (state, products) => {
    state.products = products;
  },
  newProducts: (state, products) => {
    state.products.unshift(products);
  },
  removeProduct: () => {
    // state.products.filter((product) => product.id !== id);
    location.reload();
  },
};

export default {
  state,
  getters,
  actions,
  mutations,
};
