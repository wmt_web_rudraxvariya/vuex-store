const state = {
  userProfile: [],
};
const getters = {
  getUsers: (state) => state.userProfile,
};
const actions = {
  setUserProfile({ commit }, payload) {
    commit("setUserState", payload);
  },
};
const mutations = {
  setUserState(state, user) {
    state.userProfile = user;
  },
};
export default {
  state,
  getters,
  actions,
  mutations,
};
