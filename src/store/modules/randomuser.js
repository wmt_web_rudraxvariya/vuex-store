import axios from "axios";

const state = {
  user: [],
};
const getters = {
  randomUsers: (state) => state.user,
};

const actions = {
  async getUser({ commit }) {
    console.log("api fetching..");
    const response = await axios.get("https://randomuser.me/api");
    commit("setUser", response.data.results[0]);
  },
};
const mutations = {
  setUser: (state, userDetail) => {
    const AllUser = state.user;
    state.user = [...AllUser, userDetail];
    console.log(state.user);
  },
};
export default {
  state,
  getters,
  actions,
  mutations,
};
