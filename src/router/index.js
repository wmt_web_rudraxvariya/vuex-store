import { createRouter, createWebHistory } from "vue-router";
import Home from "../view/Home.vue";
import Profile from "../view/Profile.vue";
import Prediction from "../view/Prediction.vue";
import Product from "../view/Product.vue";

const routes = [
  {
    name: "Home",
    path: "/",
    component: Home,
  },
  {
    name: "Profile",
    path: "/profile",
    component: Profile,
  },
  {
    name: "Prediction",
    path: "/prediction",
    component: Prediction,
  },
  {
    name: "Product",
    path: "/product",
    component: Product,
  },
];
const router = createRouter({
  history: createWebHistory(),
  routes,
});
export default router;
